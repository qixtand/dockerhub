#!/bin/bash
set -e

PG_LOG=/var/log/postgresql/
PG_USER=postgres
PG_CONFIG=/etc/pgbouncer/pgbouncer.ini

mkdir -p ${PG_LOG}
chmod -R 755 ${PG_LOG}
chown -R ${PG_USER}:${PG_USER} ${PG_LOG}

exec pgbouncer -q -u ${PG_USER} ${PG_CONFIG}
