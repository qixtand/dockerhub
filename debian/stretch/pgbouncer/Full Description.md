# Tags and respective Dockerfile links

- [`qixtand/debian-stretch-pgbouncer:1.7.2` (*debian/stretch/pgbouncer/1.7.2/Dockerfile*)](https://gitlab.com/qixtand/dockerhub/blob/master/debian/stretch/pgbouncer/1.7.2/Dockerfile)
- [`qixtand/debian-stretch-pgbouncer:master` (*debian/stretch/pgbouncer/master/Dockerfile*)](https://gitlab.com/qixtand/dockerhub/blob/master/debian/stretch/pgbouncer/master/Dockerfile)

# Quick reference
-	**Where to file issues**:
	[https://gitlab.com/qixtand/dockerhub/issues](https://gitlab.com/qixtand/dockerhub/issues)

-	**Maintained by**:
	[Jesse Washburn](https://gitlab.com/qixtand/dockerhub)

-	**Supported Docker versions**:
	[the latest release](https://github.com/docker/docker-ce/releases/latest) (down to 1.6 on a best-effort basis)

# How to use this image

## Create a `Dockerfile` in your project directory

```dockerfile
FROM qixtand/debian-stretch-pgbouncer

COPY pgbouncer.ini /etc/pgbouncer/pgbouncer.ini
COPY userlist.txt /etc/pgbouncer/userlist.txt
```

Put this file in the root of your project.

You can then build and run the image:

```console
$ docker build -t my-app .
$ docker run -it --name my-running-script my-app
```
