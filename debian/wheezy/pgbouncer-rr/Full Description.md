# Tags and respective Dockerfile links

- [`qixtand/debian-wheezy-pgbouncer-rr:1.7.2` (*debian/wheezy/pgbouncer-rr/1.7.2/Dockerfile*)](https://gitlab.com/qixtand/dockerhub/blob/master/debian/wheezy/1.7.2/pgbouncer-rr/Dockerfile)
- [`qixtand/debian-wheezy-pgbouncer-rr:master` (*debian/wheezy/pgbouncer-rr/master/Dockerfile*)](https://gitlab.com/qixtand/dockerhub/blob/master/debian/wheezy/master/pgbouncer-rr/Dockerfile)

# Quick reference
-	**Where to file issues**:
	[https://gitlab.com/qixtand/dockerhub/issues](https://gitlab.com/qixtand/dockerhub/issues)

-	**Maintained by**:
	[Jesse Washburn](https://gitlab.com/qixtand/dockerhub)

-	**Supported Docker versions**:
	[the latest release](https://github.com/docker/docker-ce/releases/latest) (down to 1.6 on a best-effort basis)

# How to use this image

## Create a `Dockerfile` in your project directory

```dockerfile
FROM qixtand/debian-wheezy-pgbouncer

COPY pgbouncer.ini /etc/pgbouncer/pgbouncer.ini
COPY userlist.txt /etc/pgbouncer/userlist.txt
COPY rewrite_query.py /etc/pgbouncer/rewrite_query.py
COPY routing_rules.py /etc/pgbouncer/routing_rules.py
```

Put this file in the root of your project.

You can then build and run the image:

```console
$ docker build -t my-app .
$ docker run -it --name my-running-script my-app
```
