# Tags and respective Dockerfile links

- [`qixtand/raspbian-stretch-curl` (*raspbian/stretch/curl/Dockerfile*)](https://gitlab.com/qixtand/dockerhub/blob/master/raspbian/stretch/curl/Dockerfile)

# Quick reference
-	**Where to file issues**:
	[https://gitlab.com/qixtand/dockerhub/issues](https://gitlab.com/qixtand/dockerhub/issues)

-	**Maintained by**:
	[Jesse Washburn](https://gitlab.com/qixtand/dockerhub)

-	**Supported Docker versions**:
	[the latest release](https://github.com/docker/docker-ce/releases/latest) (down to 1.6 on a best-effort basis)

# How to use this image

## Create a `Dockerfile` in your project directory

```dockerfile
FROM qixtand/raspbian-stretch-curl
CMD ["./your-daemon-or-script.sh"]
```

Put this file in the root of your project.

You can then build and run the image:

```console
$ docker build -t my-app .
$ docker run -it --name my-running-script my-app
```
